//
//  LentaTableViewCell.h
//  Bottle
//
//  Created by Roman Kovrigin on 09.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LentaTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *brand;
@property (weak, nonatomic) IBOutlet UITextView *info;
@property (weak, nonatomic) IBOutlet UIImageView *baner;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@end
