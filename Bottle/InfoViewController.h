//
//  InfoViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 05.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface InfoViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate>
{
@public
    id info;
    CLLocationCoordinate2D coordinates;
}
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end
