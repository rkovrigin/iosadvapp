//
//  ShopTableViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 06.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageTableViewCell.h"
#import "HttpWorker.h"

@interface ShopTableViewController : UITableViewController
{
@public
    id jsonAdv;
    id jsonCoordinates;
    NSString *title;
    UIImage *mainImage;
    NSArray *nib;
    CGRect textViewSize;
    
    HttpWorker *httpWorker;
@private
    UIActivityIndicatorView *spinner;
}
- (CGRect) getCorrectImageRect:(UIImage *)image;
//- (void) setImageToCell:(ImageTableViewCell *)imageCell withImage:(UIImage *)image;
@end
