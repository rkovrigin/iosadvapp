//
//  ImageTableViewCell.h
//  Bottle
//
//  Created by Roman Kovrigin on 06.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTableViewCell : UITableViewCell
{
@private
    CGRect rect;
    BOOL isRectFixed;
}

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;

- (void) setRectAndFix:(CGRect)rect;
- (CGRect) getRect;
@end
