//
//  LentaTableViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 05.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HttpWorker.h"

@interface LentaTableViewController : UITableViewController<UISearchResultsUpdating, UISearchBarDelegate, CLLocationManagerDelegate, HttpWorkerDelegate>
{
//    NSMutableArray *jsonList;
//    NSMutableArray *brandImagesArray;
    NSMutableDictionary *mallsWithAdv;
    NSMutableDictionary *filteredMallsWithAdv;
    NSMutableDictionary *uidImages;
    UISearchController *lentaSearchController;
    NSMutableArray* sortedKeys;
    NSMutableArray* sortedFilteredKeys;
    CLLocation *home;
    CLLocationManager *locationManager;
    NSDictionary *_selectedMallID;
    NSDictionary *_selectedCityID;
    NSMutableArray *cities;
    
    HttpWorker* httpWorker;
    NSString* _searchText;
}

- (NSInteger)differenceInDaysWithTodayAnd:(NSString *)date;
- (void)filterWith:(NSString*)text;
- (BOOL)checkTags:(NSArray*)tags matchWith:(NSString*)text;
- (BOOL)checkBrand:(NSString*)brand matchWith:(NSString*)text;
- (void)sortMallsByDistance:(NSMutableDictionary*)mallsDict;
- (void)startStandardUpdates;
@end
