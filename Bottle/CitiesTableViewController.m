//
//  CitiesTableViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 24.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "CitiesTableViewController.h"

@interface CitiesTableViewController ()

@end

@implementation CitiesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    httpWorker = [[HttpWorker alloc] init];
    httpWorker.delegate = self;
    
    self.title = @"Choose City";
    
    cities = [[NSMutableArray alloc] init];
    NSString *url = [httpWorker getUrlOfCities];
    [httpWorker getMallsWithURL:url];
}

- (void) refreshPageArray:(NSMutableArray *)jsonArray
{
    [self->cities removeAllObjects];
    self->cities = jsonArray;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return [cities count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"city" forIndexPath:indexPath];
    if (indexPath.row < cities.count)
    {
//        NSLog(@"%@", [cities objectAtIndex:indexPath.row]);
        cell.textLabel.text = [[cities objectAtIndex:indexPath.row] objectForKey:@"ru"];
        NSString *_id = [[cities objectAtIndex:indexPath.row] objectForKey:@"_id"];
        if ([_id isEqualToString:[_selectedCityID objectForKey:@"_id"]])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
    }
    return cell;
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return sectionName;
//}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    _selectedCityID = [cities objectAtIndex:[indexPath row]];
//    NSLog(@"CIty: %@", _selectedCityID);
//}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *selectedPath = [self.tableView indexPathForCell:sender];
    [self.tableView deselectRowAtIndexPath:selectedPath animated:YES];
    _selectedCityID = [cities objectAtIndex:[selectedPath row]];
    NSLog(@"Chosen city: [%@]", [_selectedCityID objectForKey:@"eng"]);
}


@end
