//
//  HttpWorker.m
//  Bottle
//
//  Created by Roman Kovrigin on 24/12/2016.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//


/************************************************/
/**@"http://localhost:8080";*********************/
/**@"http://93.171.216.117:8080";****************/
/************************************************/

#import <Foundation/Foundation.h>
#import "HttpWorker.h"

@implementation HttpWorker

- (void)getMallsWithURL:(NSString *)url
{
    NSMutableArray *mallList = [[NSMutableArray alloc] init];

    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                __block NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                for (int i = 0; i < [jsonArray count]; ++i)
                {
                    NSDictionary* json = jsonArray[i];
                    [mallList addObject:json];
                }
                
                if ([self.delegate respondsToSelector:@selector(refreshPageArray:)]) {
                    [self.delegate refreshPageArray:mallList];
                }
            }] resume];
}

- (void)getAdvertsWithURL:(NSString *)url
{
    NSMutableDictionary *malls = [[NSMutableDictionary alloc] init];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                __block NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                for (int i = 0; i < [jsonArray count]; ++i)
                {
                    NSDictionary* json = jsonArray[i];
                    NSString *mall = [[json objectForKey:@"advMall"]objectForKey:@"id"];
                    if ([malls objectForKey:mall] == nil)
                    {
                        [malls setValue:[[NSMutableArray alloc] init] forKey:mall];
                    }
                    if ([[malls objectForKey:mall] indexOfObject:json] == NSNotFound)
                    {
                        [[malls objectForKey:mall] addObject:json];
                    }
                }
                if ([self.delegate respondsToSelector:@selector(refreshPageDictionary:)]) {
                    [self.delegate refreshPageDictionary:malls];
                }
            }] resume];
}

- (void)increaseSegueCount:(NSDictionary *)adv
{
    NSString *url = [self getUrlSegueIncrease:adv];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error)
      {
      }] resume];
}

- (void)sendUserRequestText:(NSString *)text andLocation:(CLLocation *)coord
{
    NSURLSession *session = [NSURLSession sharedSession];

    NSURL *url = [NSURL URLWithString:[self getUrlUserRequestText:text andLocation:coord]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask =
        [session dataTaskWithRequest:request completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
    }];
    
    [postDataTask resume];
}

- (NSString *) getServerAddressWithPort
{
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"UserSettings" ofType:@"plist"];
    NSDictionary *plist = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    NSString* address = [[NSString alloc] initWithFormat:@"http://%@:%@", [plist objectForKey:@"ip"], [plist objectForKey:@"port"]];
    return address;
}

- (NSString*) getUrlOfMallsWithCity:(NSDictionary*)city
{
    NSString *server = [self getServerAddressWithPort];
    NSString *url = url = [[NSString alloc] initWithFormat:@"%@/malls/%@", server, [city objectForKey:@"_id"]];
    return url;
}

- (NSString*) getUrlOfAdvertsAtCity:(NSDictionary*)city andMall:(NSDictionary*)mall
{
    NSString *server = [self getServerAddressWithPort];
    NSString *url;
    if (mall == nil || [mall count] == 0)
    {
        url = [[NSString alloc] initWithFormat:@"%@/adv/city/%@", server, [city objectForKey:@"_id"]];
    }
    else
    {
        url = [[NSString alloc] initWithFormat:@"%@/adv/city/%@/mall/%@", server, [city objectForKey:@"_id"], [mall objectForKey:@"_id"]];
    }
    return url;
}

- (NSString*) getUrlOfCities
{
    NSString *server = [self getServerAddressWithPort];
    NSString *url = [[NSString alloc] initWithFormat:@"%@/cities",server];
    return url;
}

- (NSString*) getUrlSegueIncrease:(NSDictionary*)adv
{
    NSString *server = [self getServerAddressWithPort];
    NSString *url = [[NSString alloc] initWithFormat:@"%@/adv/%@", server, [adv objectForKey:@"_id"]];
    return url;
}

- (NSString*) getUrlUserRequestText:(NSString *)text andLocation:(CLLocation *)coord
{
    NSString *server = [self getServerAddressWithPort];
    NSString *url = [[NSString alloc] initWithFormat:
                     @"%@/request?request=%@&latitude=%f&longitude=%f", server, text, coord.coordinate.latitude, coord.coordinate.longitude];
    return url;
}
@end
