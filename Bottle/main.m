//
//  main.m
//  Bottle
//
//  Created by Roman Kovrigin on 12.03.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
