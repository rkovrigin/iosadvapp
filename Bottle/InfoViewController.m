//
//  InfoViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 05.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setPinForLatitue];
    
    MKCoordinateRegion mapRegion;
    mapRegion.center.latitude = coordinates.latitude;
    mapRegion.center.longitude = coordinates.longitude;
    mapRegion.span.latitudeDelta = 0.01;
    mapRegion.span.longitudeDelta = 0.01;
    [self.map setRegion:mapRegion animated: YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPinForLatitue
{
    NSLog(@"PIN INFO");
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = coordinates;
    annotationPoint.title = [info objectForKey:@"Name"];
    annotationPoint.subtitle = [info objectForKey:@"Info"];
    [self.map addAnnotation:annotationPoint];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
