//
//  HttpWorker.h
//  Bottle
//
//  Created by Roman Kovrigin on 24/12/2016.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#ifndef HttpWorker_h
#define HttpWorker_h

#import <MapKit/MapKit.h>

@protocol HttpWorkerDelegate<NSObject>
@optional
- (void) refreshPageArray:(NSMutableArray*)jsonArray;
@optional
- (void) refreshPageDictionary:(NSMutableDictionary*)jsonDictionary;
@end

@interface HttpWorker : NSObject
@property (nonatomic, weak) id <HttpWorkerDelegate> delegate;
- (void)getMallsWithURL:(NSString *)url;
- (void)getAdvertsWithURL:(NSString *)url;
- (void)increaseSegueCount:(NSString *)url;
- (void)sendUserRequestText:(NSString *)text andLocation:(CLLocation*)coord;

- (NSString*) getServerAddressWithPort;
- (NSString*) getUrlOfAdvertsAtCity:(NSDictionary*)city andMall:(NSDictionary*)mall;
- (NSString*) getUrlOfMallsWithCity:(NSDictionary*)city;
- (NSString*) getUrlSegueIncrease:(NSDictionary*)adv;
- (NSString*) getUrlOfCities;
- (NSString*) getUrlUserRequestText:(NSString *)text andLocation:(CLLocation *)coord;
@end


#endif /* HttpWorker_h */
