//
//  TextTableViewCell.h
//  Bottle
//
//  Created by Roman Kovrigin on 08.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextTableViewCell : UITableViewCell
{
    CGRect rect;
    BOOL fitted;
}
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (CGRect) getFitRect;
- (void)textViewDidChange;
@end
