//
//  LentaTableViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 05.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "LentaTableViewController.h"
#import "InfoViewController.h"
#import "ShopTableViewController.h"
#import "LentaTableViewCell.h"
#import "MallsTableViewController.h"

@interface LentaTableViewController ()

@end

@implementation LentaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    httpWorker = [[HttpWorker alloc] init];
    httpWorker.delegate = self;
    
    _selectedMallID = [[NSMutableDictionary alloc] init];
    _selectedCityID = [[NSMutableDictionary alloc] init];
    
    if ((_selectedMallID == nil) || ([_selectedMallID count] == 0))
    {
        self.title = @"ALL Malls";
    }
    else
    {
        [_selectedMallID objectForKey:@"mall"];
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"UserSettings" ofType:@"plist"];
    NSDictionary *settings = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSLog(@"%@", settings);
    cities = [[NSMutableArray alloc] init];
    mallsWithAdv = [[NSMutableDictionary alloc] init];
    filteredMallsWithAdv = [[NSMutableDictionary alloc] init];
    uidImages = [[NSMutableDictionary alloc] init];
    sortedKeys = [[NSMutableArray alloc] init];
    sortedFilteredKeys = [[NSMutableArray alloc] init];
    
    [self startStandardUpdates];
    [self.tableView registerNib:[UINib nibWithNibName:@"LentaTableViewCellXib" bundle:nil] forCellReuseIdentifier:@"LentaTableViewCell"];
    lentaSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    lentaSearchController.searchResultsUpdater = self;
    lentaSearchController.dimsBackgroundDuringPresentation = false;
    lentaSearchController.definesPresentationContext = TRUE;
    lentaSearchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = lentaSearchController.searchBar;
}

- (void) refreshPageArray:(NSMutableArray *)jsonArray
{
    [self->cities removeAllObjects];
    self->cities = jsonArray;
    [self afterCitiesLoded];
}

- (void) refreshPageDictionary:(NSMutableDictionary *)jsonDictionary
{
    [self->mallsWithAdv removeAllObjects];
    self->mallsWithAdv = jsonDictionary;
    [self sortMallsByDistance:mallsWithAdv];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar // called when text ends editing
{
    if ([lentaSearchController.searchBar.text length] == 0)
    {
        [httpWorker sendUserRequestText:_searchText andLocation:locationManager.location];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([lentaSearchController.searchBar.text length] == 0)
    {
        [httpWorker sendUserRequestText:_searchText andLocation:locationManager.location];
    }
}

- (void) afterCitiesLoded
{
    CLLocationDistance tmp = FLT_MAX;
    for (NSDictionary *city in cities) {
        double latitude = [[city objectForKey:@"latitude"] doubleValue];
        double longitude = [[city objectForKey:@"longitude"] doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocationDistance dist = [home distanceFromLocation:loc];
        if (dist < tmp)
        {
            _selectedCityID = city;
            tmp = dist;
        }
    }
    NSString *url = [httpWorker getUrlOfAdvertsAtCity:_selectedCityID andMall:_selectedMallID];
    [self->httpWorker getAdvertsWithURL:url];
}

- (void)startStandardUpdates
{
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)startSignificantChangeUpdates
{
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    [locationManager startMonitoringSignificantLocationChanges];
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (fabs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"_0_latitude %+.6f, longitude %+.6f\n", location.coordinate.latitude, location.coordinate.longitude);
        home = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
        if(_selectedCityID == nil || [_selectedCityID count] == 0)
        {
            NSString *cityUrl = [httpWorker getUrlOfCities];
            [self->httpWorker getMallsWithURL:cityUrl];
        }
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchText = lentaSearchController.searchBar.text;
    [self filterWith:searchText.lowercaseString];
    _searchText = searchText;
    
//    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
//    });
}

- (void) filterWith:(NSString *)text
{
    [filteredMallsWithAdv removeAllObjects];
    [sortedFilteredKeys removeAllObjects];

    if ([sortedKeys count] > 0)
    {
        for (NSString* mallName in sortedKeys)
        {
            id adverts = [mallsWithAdv objectForKey:mallName];
            for (id adv in adverts)
            {
                NSString* brandName = [[adv objectForKey:@"advInfo"] objectForKey:@"brandName"];
                NSString* allInfo = [[adv objectForKey:@"advInfo"] objectForKey:@"allInfo"];
                NSArray* tags = [adv objectForKey:@"tags"];
                if ([self checkBrand:brandName matchWith:text] ||
                    ([allInfo.lowercaseString containsString:text]) ||
                    [self checkTags:tags matchWith:text])
                {
                    if ([filteredMallsWithAdv objectForKey:mallName] == nil)
                    {
                        [filteredMallsWithAdv setValue:[[NSMutableArray alloc] init] forKey:mallName];
                    }
                    [[filteredMallsWithAdv objectForKey:mallName] addObject:adv];
                    
                    if (![sortedFilteredKeys containsObject:mallName])
                    {
                        [sortedFilteredKeys addObject:mallName];
                    }
                }
            }
        }
    }
}

- (BOOL) checkTags:(NSArray *)tags matchWith:(NSString *)text
{
    for (int i = 0; i < tags.count; ++i)
    {
        NSString *tag = [tags[i] lowercaseString];
        if ([tag containsString:text])
        {
            return TRUE;
        }
    }
    return FALSE;
}

- (BOOL) checkBrand:(NSString *)brand matchWith:(NSString *)text
{
    NSMutableString *eng_text = [text.lowercaseString mutableCopy];
    CFMutableStringRef bufferRef_eng_text = (__bridge CFMutableStringRef)eng_text;
    CFStringTransform(bufferRef_eng_text, NULL, kCFStringTransformToLatin, false);

    NSMutableString *rus_brand = [brand.lowercaseString mutableCopy];
    CFMutableStringRef bufferRef_rus_brand = (__bridge CFMutableStringRef)rus_brand;
    CFStringTransform(bufferRef_rus_brand, NULL, kCFStringTransformLatinCyrillic, false);

    return [brand.lowercaseString containsString:text] ||
            [brand.lowercaseString containsString:eng_text] ||
            [rus_brand.lowercaseString containsString:text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ((lentaSearchController.active) && (![lentaSearchController.searchBar.text  isEqual: @""]))
    {
        return [[filteredMallsWithAdv allKeys] count];
    }
    return [[mallsWithAdv allKeys] count];
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ((lentaSearchController.active) && (![lentaSearchController.searchBar.text  isEqual: @""]))
    {
        NSString *key = [[filteredMallsWithAdv allKeys] objectAtIndex:section];
        key = sortedFilteredKeys[section];
        NSLog(@"%@ %lu", key, (long)section);
        return [[filteredMallsWithAdv objectForKey:key] count];
    }
    NSString *key = [[mallsWithAdv allKeys] objectAtIndex:section];
    key = sortedKeys[section];
    return [[mallsWithAdv objectForKey:key] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LentaTableViewCell *_cell = [tableView dequeueReusableCellWithIdentifier:@"LentaTableViewCell" forIndexPath:indexPath];
    if (_cell == nil)
    {
        NSLog(@"NEW");
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LentaTableViewCellXib" owner:self options:nil];
        _cell = [nib objectAtIndex:0];
    }
    NSString *key;
    id indexInfo;
    if ((lentaSearchController.active) && (![lentaSearchController.searchBar.text  isEqual: @""]))
    {
//        key = [[filteredMallsWithAdv allKeys] objectAtIndex:[indexPath section]];
        key = sortedFilteredKeys[[indexPath section]];
        indexInfo = [[filteredMallsWithAdv objectForKey:key] objectAtIndex:[indexPath row]];
    }
    else{
//        key = [[malls allKeys] objectAtIndex:[indexPath section]];
        key = sortedKeys[[indexPath section]];
        indexInfo = [[mallsWithAdv objectForKey:key] objectAtIndex:[indexPath row]];
    }
    
    NSString *uid = [indexInfo objectForKey:@"_id"];
    id advInfo = [indexInfo objectForKey:@"advInfo"];
    id advDate = [indexInfo objectForKey:@"advDate"];
    id advLinks = [indexInfo objectForKey:@"advLinks"];
    
    _cell.brand.text = [advInfo objectForKey:@"brandName"];
    _cell.info.text = [advInfo objectForKey:@"allInfo"];
    
    if ([[advDate objectForKey:@"showEndOfficial"] boolValue])
    {
        NSString *end = [advDate objectForKey:@"endOfficial"];
        long days = (long)[self differenceInDaysWithTodayAnd:end];
        _cell.subLabel.text = [[NSString alloc] initWithFormat:@"Осталось дней: %lu", days];
    }
    else
    {
        _cell.subLabel.text = [[NSString alloc] initWithFormat:@""];
    }
    
//    NSString *strUrl = [[advLinks objectForKey:@"lentaBanerUrl"] stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *strUrl = [advLinks objectForKey:@"lentaBanerUrl"];
    
    if ([uidImages objectForKey:uid] == nil)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSURL *url = [NSURL URLWithString:strUrl];
                NSData *data = [NSData dataWithContentsOfURL : url];
                UIImage *image = [UIImage imageWithData: data];
                if (image != nil)
                {
                    [uidImages setObject:image forKey:uid];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _cell.baner.image = image;
                    });
                }
        });
    }
    else
    {
        _cell.baner.image = [uidImages objectForKey:uid];
    }
    return _cell;
}

- (NSInteger)differenceInDaysWithTodayAnd:(NSString *)date
{
    date = [date substringToIndex:[date length]-9];
    date = [date stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *capturedStartDate = [dateFormatter dateFromString: date];
    NSDate *today = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay fromDate:today toDate:capturedStartDate options:0];
    return [difference day];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *key = sortedKeys[section];
    NSDictionary *indexInfo = [[mallsWithAdv objectForKey:key] objectAtIndex:0];
    
    if ((lentaSearchController.active) && (![lentaSearchController.searchBar.text  isEqual: @""]))
    {
        key = sortedFilteredKeys[section];
        indexInfo = [[filteredMallsWithAdv objectForKey:key] objectAtIndex:0];
    }
    else{
        key = sortedKeys[section];
        indexInfo = [[mallsWithAdv objectForKey:key] objectAtIndex:0];
    }
    
    return [[[indexInfo objectForKey:@"advMall"] objectForKey:@"name"]objectForKey:@"ru"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 167;
}

- (void)sortMallsByDistance:(NSMutableDictionary*)mallsDict
{
    NSMutableArray *distances = [[NSMutableArray alloc] init];
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
    int _dist = 0;
    
    for (int i = 0; i < [[mallsDict allKeys] count]; ++i)
    {
        NSString *key = [[mallsDict allKeys] objectAtIndex:i]; // mall IDs
        NSDictionary *coordinates = [[[[mallsDict objectForKey:key] objectAtIndex:0] objectForKey:@"advMall"]objectForKey:@"coordinates"];
        double latitude = [[coordinates objectForKey:@"latitude"] doubleValue];
        double longitude = [[coordinates objectForKey:@"longitude"] doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        if (home)
        {
            CLLocationDistance dist = [home distanceFromLocation:loc];
            [distances addObject:[NSNumber numberWithDouble:floor(dist)]];
            [tmp setObject:[NSNumber numberWithDouble:floor(dist)] forKey:key];
        }
        else
        {
            [distances addObject:[NSNumber numberWithInt:_dist]];
            [tmp setObject:[NSNumber numberWithInt:_dist] forKey:key];
            ++_dist;
        }
    }
    
    NSLog(@"%@", distances);
    
    NSArray *sorted_distances = [distances sortedArrayUsingSelector:@selector(compare:)];
    [sortedKeys removeAllObjects];
    for (int i = 0; i < [sorted_distances count]; ++i)
    {
        NSArray *_t = [tmp allKeysForObject:sorted_distances[i]];
        [sortedKeys addObject:_t[0]];
        NSLog(@"%@", _t);
    }
    
    [distances removeAllObjects];
    [tmp removeAllObjects];
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"lenta_to_shop_info_table" sender: self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"lenta_to_malls_list"])
    {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        MallsTableViewController *mallsTable = (MallsTableViewController*)navController.topViewController;
        mallsTable->_selectedCityID = _selectedCityID;
        mallsTable->_selectedMallID = _selectedMallID;
    }
    else
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSString *key;
        id json;
        if ((lentaSearchController.active) && (![lentaSearchController.searchBar.text  isEqual: @""]))
        {
            key = sortedFilteredKeys[[indexPath section]];
            json = [[filteredMallsWithAdv objectForKey:key] objectAtIndex:[indexPath row]];
        }
        else
        {
            key = sortedKeys[[indexPath section]];
            json = [[mallsWithAdv objectForKey:key] objectAtIndex:[indexPath row]];
        }
        
        [lentaSearchController setActive:NO]; //deselect search
        
        if([[segue identifier] isEqualToString:@"lenta_to_shop_info_table"])
        {
            id info = [json objectForKey:@"shopInfo"];
            ShopTableViewController *dest = segue.destinationViewController;
            dest->jsonAdv = json;
            dest->jsonCoordinates = [[json objectForKey:@"advMall"] objectForKey:@"coordinates"];
            dest->title = [[info objectForKey:@"advInfo"] objectForKey:@"brandName"];
        }
    }
}

- (IBAction)unwindToLenta:(UIStoryboardSegue*)sender
{
    if ([sender.sourceViewController isKindOfClass:[MallsTableViewController class]]) {
        MallsTableViewController *mallsListTable = sender.sourceViewController;
        if (_selectedMallID != mallsListTable->_selectedMallID || _selectedCityID != mallsListTable->_selectedCityID)
        {
            _selectedMallID = mallsListTable->_selectedMallID;
            _selectedCityID = mallsListTable->_selectedCityID;
            [mallsWithAdv removeAllObjects];
            NSString *url = [httpWorker getUrlOfAdvertsAtCity:_selectedCityID andMall:_selectedMallID];
            [self->httpWorker getAdvertsWithURL:url];
            if (_selectedMallID)
            {
                self.title = [_selectedMallID objectForKey:@"mall"];
            }
            else
            {
                self.title = @"ALL Malls";
            }
            [self.tableView reloadData];
        }
    }
}

@end
