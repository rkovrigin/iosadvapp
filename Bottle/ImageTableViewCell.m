//
//  ImageTableViewCell.m
//  Bottle
//
//  Created by Roman Kovrigin on 06.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    rect = CGRectMake(0, 0, screenRect.size.width, 48);
    isRectFixed = FALSE;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void) setRectAndFix:(CGRect)newRect
{
    self->rect = CGRectMake(0, 0, newRect.size.width, newRect.size.height);
    isRectFixed = TRUE;
}

- (CGRect) getRect
{
    return rect;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.image.frame = CGRectMake(0,0,rect.size.width,rect.size.height);
}

@end
