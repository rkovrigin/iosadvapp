//
//  ShopTableViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 06.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "ShopTableViewController.h"
#import "ImageTableViewCell.h"
#import "InfoViewController.h"
#import "TextTableViewCell.h"

@interface ShopTableViewController ()

@end

@implementation ShopTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    httpWorker = [[HttpWorker alloc] init];
    
    self.title = [[jsonAdv objectForKey:@"advInfo"] objectForKey:@"brandName"];
    nib = [[NSBundle mainBundle] loadNibNamed:@"ImageTableViewCell" owner:self options:nil];
    mainImage = NULL;
    [httpWorker increaseSegueCount:jsonAdv];//increase counter means user opened the advert
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if ([indexPath row] == 0)
    {
        NSString *strUrl = [[jsonAdv objectForKey:@"advLinks"] objectForKey:@"mainBannerUrl"];
        ImageTableViewCell* _cell;
        _cell = [nib objectAtIndex:0];
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.frame = _cell.getRect;
        [_cell addSubview:spinner];
        [spinner startAnimating];
        
        //spinner stais if during loading scroll out this sell
        if (mainImage)
        {
            [spinner stopAnimating];
            _cell.image.image = mainImage;
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSURL *url = [NSURL URLWithString:strUrl];
                    NSData *data = [NSData dataWithContentsOfURL : url];
                    UIImage *image = [UIImage imageWithData: data];
                    mainImage = image;
                    if (mainImage)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [spinner stopAnimating];
                            [_cell setRectAndFix:[self getCorrectImageRect:mainImage]];
                            _cell.image.image = mainImage;
                            [self.tableView beginUpdates];
                            [self.tableView endUpdates];
                            [_cell layoutSubviews];
                        });
                    }
            });
        }
        cell = _cell;
    }
    else if ([indexPath row] == 1){
        cell = (ImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"header"];
        cell.textLabel.text = [[jsonAdv objectForKey:@"advLinks"] objectForKey:@"coordinatesWithinMall"];
        cell.textLabel.userInteractionEnabled = TRUE;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    }
    else if ([indexPath row] == 2)
    {
        TextTableViewCell* _cell = (TextTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TextTableViewCell"];
        if (_cell == nil)
        {
            _cell = [nib objectAtIndex:1];
        }
        [_cell.textView setText: [[jsonAdv objectForKey:@"advInfo"] objectForKey:@"allInfo"]];
//        [_cell textViewDidChange];
//        [_cell layoutSubviews];
//        textViewSize = _cell.textView.frame;
        textViewSize = [_cell getFitRect];
        return _cell;
    }
    else if ([indexPath row] == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"map_segue" forIndexPath:indexPath];
        cell.textLabel.text = @"MAP";
    }
    else if ([indexPath row] == 4)
    {
        cell = (ImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"link"];
        cell.textLabel.text = @"Link";
    }
    
    return cell;
}

- (CGRect) getCorrectImageRect:(UIImage *)image
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect correctRect;
    if (image.size.width > screenRect.size.width)
    {
        CGFloat cfen = image.size.width/screenRect.size.width;
        correctRect = CGRectMake(0, 0, screenRect.size.width, image.size.height/cfen);
    }
    else
    {
        correctRect = CGRectMake(0, 0, image.size.width, image.size.height);
    }
    return correctRect;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 2)
    {
        return textViewSize.size.height;
    }
    else if ([indexPath row] == 0)
    {
        if (mainImage != NULL)
        {
            CGRect correctRect = [self getCorrectImageRect:mainImage];
            return correctRect.size.height;
        }
    }
    return 48;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[tableView cellForRowAtIndexPath:indexPath] reuseIdentifier] isEqualToString:@"link"])
    {
        NSURL *url = [NSURL URLWithString:[[jsonAdv objectForKey:@"advLinks"] objectForKey:@"officialWebSite"]];
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"shop_info_to_map"])
    {
        InfoViewController *dest = segue.destinationViewController;
        dest->coordinates = CLLocationCoordinate2DMake([[jsonCoordinates objectForKey:@"latitude"] doubleValue], [[jsonCoordinates objectForKey:@"longitude"] doubleValue]);
        dest->info = jsonAdv;
    }
}


@end
