//
//  TextTableViewCell.m
//  Bottle
//
//  Created by Roman Kovrigin on 08.04.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "TextTableViewCell.h"

@implementation TextTableViewCell

- (void)awakeFromNib {
    // Initialization code
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    fitted = NO;
    rect = CGRectMake(0,0, screenRect.size.width, 48);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
//    self.textView.frame = CGRectMake(8,0, rect.size.width, rect.size.height);
//    self.textView.textContainerInset = UIEdgeInsetsMake(10.0, 10.0, 0.0, 10.0);
//    self.textView.textContainerInset = UIEdgeInsetsMake(10.0, 0.0, 0.0, 10.0);
}

- (void)textViewDidChange
{
//    if (!fitted)
//    {
        CGFloat fixedWidth = rect.size.width;
        CGSize newSize = [self.textView sizeThatFits:CGSizeMake(fixedWidth, FLT_MAX)];
        CGRect newFrame = self.textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), self.textView.contentSize.height+20);
        self->rect = newFrame;
        fitted = YES;
//    }
}

- (CGRect) getFitRect
{
    CGFloat fixedWidth = rect.size.width;
    CGSize newSize = [self.textView sizeThatFits:CGSizeMake(fixedWidth, FLT_MAX)];
    CGRect newFrame = self.textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), self.textView.contentSize.height+20);
    self->rect = newFrame;
    return newFrame;
}

@end
