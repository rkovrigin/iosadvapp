//
//  ViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 12.03.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "ViewController.h"

#define ARC4RANDOM_MAX 0x100000000

@interface ViewController ()

@end

@implementation ViewController{
    CLLocationManager *locationManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getJsonWithStringURL:@"http://localhost:8080/figures"];
    locationManager = [[CLLocationManager alloc] init];
    [self getCurrentLocation];

////    [self setPinForLatitue:52.0991 andLongitude:108.5595];
//    CLLocationCoordinate2D coordinates[4];
//    //                                         latitude    longitude
//    coordinates[0] = CLLocationCoordinate2DMake(59.931888, 30.346851);
//    coordinates[1] = CLLocationCoordinate2DMake(59.931850, 30.347505);
//    coordinates[2] = CLLocationCoordinate2DMake(59.931329, 30.347516);
//    coordinates[3] = CLLocationCoordinate2DMake(59.931743, 30.347317);
//    
//    NSMutableArray *randPoints = [self getPoints:10 withinPolygon:coordinates withNVertices:4];
//    CLLocationCoordinate2D coordinates_2[10];
//    for(int i = 0; i < 10; i++)
//    {
//        CLLocation *loc = [randPoints objectAtIndex:i];
//        coordinates_2[i] = CLLocationCoordinate2DMake(loc.coordinate.latitude, loc.coordinate.longitude);
//    }
    
//    MKCircle *circle = [MKCircle circleWithCenterCoordinate:annotationCoord radius:40000];
//    [self.map addOverlay:circle];
//    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:2];
//    [self.map addOverlay:polyLine];
//    MKPolygon *polygon = [MKPolygon polygonWithCoordinates:coordinates count:4];
//    [self.map addOverlay:polygon];
//    
//    MKCoordinateRegion mapRegion;
//    mapRegion.center.latitude = coordinates[0].latitude;
//    mapRegion.center.longitude = coordinates[0].longitude;
//    mapRegion.span.latitudeDelta = 0.001;
//    mapRegion.span.longitudeDelta = 0.001;
//    [self.map setRegion:mapRegion animated: YES];
}

- (void)getCurrentLocation
{
    NSLog(@"getCurrentLocation");
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"didUpdateLocations");
    CLLocation *currentLocation = [locations lastObject];
    
    if (currentLocation != nil) {
        self.longitude.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        self.latitude.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)setPinForLatitue:(double)latitude andLongitude:(double)longitude
{
    CLLocationCoordinate2D annotationCoord;
    
    annotationCoord.latitude = latitude;
    annotationCoord.longitude = longitude;
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = @"Kovrigin";
    annotationPoint.subtitle = @"Kovrigin's home";
    [self.map addAnnotation:annotationPoint];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        
        renderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        renderer.lineWidth   = 3;
        
        return renderer;
    }
    else if ([overlay isKindOfClass:[MKCircle class]])
    {
        MKCircleRenderer *renderer = [[MKCircleRenderer alloc] initWithCircle:overlay];
        
        renderer.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        renderer.strokeColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
        renderer.lineWidth = 2;
        return renderer;
    }
    else if ([overlay isKindOfClass:[MKPolygon class]])
    {
        MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:overlay];
        
        renderer.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        renderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        renderer.lineWidth = 3;
        
        return renderer;
    }
    
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) drawPolygon:(NSString *)polygonString
{
    NSData *tmpData = [polygonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:tmpData options:0 error:nil];
    NSArray *coords = [json objectForKey:@"coords"];
    CLLocationCoordinate2D coordinates[[coords count]];
    for (int i = 0; i < [coords count]; ++i)
    {
        double latitude = [[coords[i] objectForKey:@"latitude"] doubleValue];
        double longitude = [[coords[i] objectForKey:@"longitude"] doubleValue];
        coordinates[i] = CLLocationCoordinate2DMake(latitude, longitude);
    }
    MKPolygon *polygon = [MKPolygon polygonWithCoordinates:coordinates count:[coords count]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.map addOverlay:polygon];
    });
}

- (void) drawRect:(NSString *)polygonString
{
    NSData *tmpData = [polygonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:tmpData options:0 error:nil];
    id json_leftbottom = [json objectForKey:@"leftbottom"];
    id json_righttop = [json objectForKey:@"righttop"];
    
    CLLocation *leftbottom = [[CLLocation alloc] initWithLatitude:[[json_leftbottom objectForKey:@"latitude"] doubleValue] longitude:[[json_leftbottom objectForKey:@"longitude"] doubleValue]];
    CLLocation *righttop = [[CLLocation alloc] initWithLatitude:[[json_righttop objectForKey:@"latitude"] doubleValue] longitude:[[json_righttop objectForKey:@"longitude"] doubleValue]];
    CLLocation *lefttop = [[CLLocation alloc] initWithLatitude:(righttop.coordinate.latitude) longitude:(leftbottom.coordinate.longitude)];
    CLLocation *rightbottom = [[CLLocation alloc] initWithLatitude:(leftbottom.coordinate.latitude) longitude:(righttop.coordinate.longitude)];
    CLLocationCoordinate2D coordinates[4];
    coordinates[0] = CLLocationCoordinate2DMake(leftbottom.coordinate.latitude, leftbottom.coordinate.longitude);
    coordinates[1] = CLLocationCoordinate2DMake(lefttop.coordinate.latitude, lefttop.coordinate.longitude);
    coordinates[2] = CLLocationCoordinate2DMake(righttop.coordinate.latitude, righttop.coordinate.longitude);
    coordinates[3] = CLLocationCoordinate2DMake(rightbottom.coordinate.latitude, rightbottom.coordinate.longitude);
    
    MKPolygon *rect = [MKPolygon polygonWithCoordinates:coordinates count:4];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.map addOverlay:rect];
    });
}

- (void) drawCircle:(NSString *)polygonString
{
    NSData *tmpData = [polygonString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:tmpData options:0 error:nil];
    id json_point = [json objectForKey:@"point"];
    CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake([[json_point objectForKey:@"latitude"] doubleValue], [[json_point objectForKey:@"longitude"] doubleValue]);
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:annotationCoord radius:[[json objectForKey:@"radius"] doubleValue]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.map addOverlay:circle];
    });
}

- (void) drawPoint:(NSString *)pointString
{
    NSData *tmpData = [pointString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:tmpData options:0 error:nil];
    id json_point = [json objectForKey:@"point"];
//    CLLocationCoordinate2D annotationCoord = CLLocationCoordinate2DMake([[json_point objectForKey:@"latitude"] doubleValue], [[json_point objectForKey:@"longitude"] doubleValue]);
}

- (void)getJsonWithStringURL:(NSString *)url
{

    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                __block NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions
                                                error:&error];
                for (int i = 0; i < [jsonArray count]; ++i)
                {
                    NSData *tmpData = [jsonArray[i] dataUsingEncoding:NSUTF8StringEncoding];
                    id json = [NSJSONSerialization JSONObjectWithData:tmpData options:0 error:nil];
                    if ([[json objectForKey:@"type"] isEqualToString:@"polygon"])
                        [self drawPolygon:jsonArray[i]];
                    else if ([[json objectForKey:@"type"] isEqualToString:@"rect"])
                        [self drawRect:jsonArray[i]];
                    else if ([[json objectForKey:@"type"] isEqualToString:@"circle"])
                        [self drawCircle:jsonArray[i]];
                    else if ([[json objectForKey:@"type"] isEqualToString:@"point"])
                        [self drawPoint:jsonArray[i]];
                }
            }] resume];
}

- (NSMutableArray*)getPoints:(int)amount withinPolygon:(CLLocationCoordinate2D*)polygon withNVertices:(int)count
{
    NSMutableArray *points = [[NSMutableArray alloc] init];
    double left = 1000; // longitude
    double right = -1000; // longitude
    double top = -1000; // latitude
    double bottom = 1000; // latitude
    
    for (int i = 0; i < count; ++i)
    {
        if (polygon[i].latitude > top)
            top = polygon[i].latitude;
        if (polygon[i].latitude < bottom)
            bottom = polygon[i].latitude;
        if (polygon[i].longitude > right)
            right = polygon[i].longitude;
        if (polygon[i].longitude < left)
            left = polygon[i].longitude;
    }
    
    while([points count] < amount)
    {
        double val_lat = ((double)arc4random() / ARC4RANDOM_MAX)
        * (top - bottom)
        + bottom;
        double val_long = ((double)arc4random() / ARC4RANDOM_MAX)
        * (right - left)
        + left;
        CLLocation *location = [[CLLocation alloc] initWithLatitude:val_lat longitude:val_long];
        
        if ([self isPoint:location withinPolygon:polygon withNVertices:count])
        {
            [points addObject: location];
            [self setPinForLatitue:location.coordinate.latitude andLongitude:location.coordinate.longitude];
            NSLog(@"%@ %d", location, [self isPoint:location withinPolygon:polygon withNVertices:count]);
        }
    }
    
    return points;
}

- (BOOL) isPoint:(CLLocation*)point withinPolygon:(CLLocationCoordinate2D*)polygon withNVertices:(int)count
{
    BOOL c = false;
    int i,j;
    
    for (i = 0, j = count - 1; i < count; j = i++)
    {
        if ( ( (polygon[i].latitude >= point.coordinate.latitude) != (polygon[j].latitude >= point.coordinate.latitude)) &&
            (point.coordinate.longitude <= (polygon[j].longitude - polygon[i].longitude) * (point.coordinate.latitude - polygon[i].latitude) /
             (polygon[j].latitude - polygon[i].latitude) + polygon[i].longitude))
        {
            c = !c;
        }
    }
    return c;
}

@end
