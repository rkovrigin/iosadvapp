//
//  ViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 12.03.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *latitude;
@property (weak, nonatomic) IBOutlet UILabel *longitude;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@end

