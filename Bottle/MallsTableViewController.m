//
//  MallsTableViewController.m
//  Bottle
//
//  Created by Roman Kovrigin on 12.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "MallsTableViewController.h"
#import "CitiesTableViewController.h"

@interface MallsTableViewController ()

@end

@implementation MallsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    httpWorker = [[HttpWorker alloc] init];
    httpWorker.delegate = self;
    
    _previousSelectedMallID = _selectedMallID;
    _previousSelectedCityID = _selectedCityID;
    
    localMallList = [[NSMutableArray alloc] init];
    NSString *url = [httpWorker getUrlOfMallsWithCity:_selectedCityID];
    [self->httpWorker getMallsWithURL:url];
}

- (void) refreshPageArray:(NSMutableArray *)jsonArray
{
    [localMallList removeAllObjects];
    localMallList = jsonArray;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([localMallList count] == 0)
        return 2;
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2)
        return [localMallList count];
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"city" forIndexPath:indexPath];
        cell.textLabel.text = [_selectedCityID objectForKey:@"ru"];
    }
    else if (indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mall" forIndexPath:indexPath];
        cell.textLabel.text = @"All";
        if ((_selectedMallID == nil) || ([_selectedMallID count] == 0))
//        if ([selectedMallID isEqualToString:ALLMALLS])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if (indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mall" forIndexPath:indexPath];
        if ([localMallList count] > 0)
        {
            cell.textLabel.text = [[localMallList objectAtIndex:[indexPath row]] objectForKey:@"mall"];
            NSString* _id = [[localMallList objectAtIndex:[indexPath row]] objectForKey:@"_id"];
//            NSString* mall = [[localMallList objectAtIndex:[indexPath row]] objectForKey:@"mall"];
            
            if ((_selectedMallID == nil) || ([_selectedMallID count] == 0))
//            if ([selectedMallID isEqualToString:ALLMALLS])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                if ([[_selectedMallID objectForKey:@"_id"] isEqualToString:_id])
//                if ([selectedMallID isEqualToString:_id])
                {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else
                {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    
    return cell;
}

- (void)setAccessoryTypeToAllMallCells:(UITableViewCellAccessoryType)accessoryType toSection:(long)section
{
    for (int i = 0; i < [localMallList count]; ++i) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [[self.tableView cellForRowAtIndexPath:indexPath] setAccessoryType:accessoryType];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
        [self setAccessoryTypeToAllMallCells:UITableViewCellAccessoryCheckmark toSection:2];
//        selectedMallID = ALLMALLS;
        _selectedMallID = nil;
    }
    else if (indexPath.section == 2)
    {
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [[self.tableView cellForRowAtIndexPath:_indexPath] setAccessoryType:UITableViewCellAccessoryNone];
        [self setAccessoryTypeToAllMallCells:UITableViewCellAccessoryNone toSection:2];
        [[self.tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
//        selectedMallID = [[localMallList objectAtIndex:[indexPath row]] objectForKey:@"_id"];
        _selectedMallID = [localMallList objectAtIndex:[indexPath row]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == _cancelButton)
    {
        _selectedMallID = _previousSelectedMallID;
        _selectedCityID = _previousSelectedCityID;
    }
    else if (sender == _doneButton)
    {
//        NSLog(@"DONE");
    }
    else if ([[segue identifier] isEqualToString:@"malls_list_to_cities_list"])
    {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        CitiesTableViewController *citiesTable = (CitiesTableViewController*)navController.topViewController;
        citiesTable->_selectedCityID = _selectedCityID;
    }
}

- (IBAction)unwindToMalls:(UIStoryboardSegue*)sender
{
    if ([sender.sourceViewController isKindOfClass:[CitiesTableViewController class]])
    {
        CitiesTableViewController *citiesListTable = sender.sourceViewController;
        if (!([[_selectedCityID objectForKey:@"_id"] isEqualToString:[citiesListTable->_selectedCityID objectForKey:@"_id"]]))
        {
            _selectedCityID = citiesListTable->_selectedCityID;
            _selectedMallID = nil;
            [localMallList removeAllObjects];
            [self.tableView reloadData];
            NSString *url = [httpWorker getUrlOfMallsWithCity:_selectedCityID];
            [self->httpWorker getMallsWithURL:url];
        }
    }
}

@end
