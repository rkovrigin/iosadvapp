//
//  CitiesTableViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 24.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWorker.h"

@interface CitiesTableViewController : UITableViewController<HttpWorkerDelegate>
{
@public
    NSMutableArray* cities;

    NSDictionary* _selectedCityID;
    NSDictionary* _previousSelectedCityID;
    
    HttpWorker* httpWorker;
}

@end
