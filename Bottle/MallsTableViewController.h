//
//  MallsTableViewController.h
//  Bottle
//
//  Created by Roman Kovrigin on 12.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWorker.h"

#define ALLMALLS @"allmalls"

@interface MallsTableViewController : UITableViewController<HttpWorkerDelegate>
{
@public
    NSMutableArray* localMallList;
//    NSString* selectedMallID;
//    NSString* selectedCityID;
//    NSString* previousSelectedMallID;
    
    NSDictionary* _selectedMallID;
    NSDictionary* _selectedCityID;
    NSDictionary* _previousSelectedMallID;
    NSDictionary* _previousSelectedCityID;
    
    HttpWorker* httpWorker;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@end
