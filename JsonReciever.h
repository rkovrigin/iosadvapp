//
//  JsonReciever.h
//  Bottle
//
//  Created by Roman Kovrigin on 14.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define REALSERVER = @"http://93.171.216.117:8080"
//#define LOCALHOST = @"http://localhost:8080"
//
//#define ADV_LOCALHOST @"http://localhost:8080/adv/city"
//#define ADV_REALSERVER @"http://93.171.216.117:8080/adv/city"
//
//#define MALLS_LOCALHOST @"http://localhost:8080/malls"
//#define MALLS_REALSERVER @"http://93.171.216.117:8080/malls"
//
//#define CITIES_LOCALHOST @"http://localhost:8080/cities"
//#define CITIES_REALSERVER @"http://93.171.216.117:8080/cities"
//
//#define __SERVER__ LOCALHOST
//#define __ADV__ ADV_LOCALHOST
//#define __MALLS__ MALLS_LOCALHOST
//#define __CITIES__ CITIES_LOCALHOST

@interface JsonReciever : NSObject

+ (void)getMallsWithURL:(NSString *)url putTo:(NSMutableArray*)mallList callBack:(void(^)())blockCallBack;
+ (void)getAdvertsWithURL:(NSString *)url putTo:(NSMutableDictionary*)malls callBack:(void(^)())blockCallBack;
+ (void)getAdvertsWithURL:(NSString *)url selectOnly:(NSString*)mallID putTo:(NSMutableDictionary*)malls callBack:(void(^)())blockCallBack;
+ (void)increaseSegueCount:(NSString *)url;

+ (NSString*) getUrlOfAdvertsAtCity:(NSDictionary*)city andMall:(NSDictionary*)mall;
+ (NSString*) getUrlOfMallsWithCity:(NSDictionary*)city;
+ (NSString*) getUrlSegueIncrease:(NSDictionary*)adv;
+ (NSString*) getUrlOfCities;
@end
