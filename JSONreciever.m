//
//  JsonReciever.m
//  Bottle
//
//  Created by Roman Kovrigin on 14.05.16.
//  Copyright © 2016 rkovrigin co. All rights reserved.
//

#import "JsonReciever.h"
#import <UIKit/UIKit.h>
#import "MallsTableViewController.h"

@implementation JsonReciever

+ (void)getMallsWithURL:(NSString *)url putTo:(NSMutableArray*)mallList callBack:(void(^)())blockCallBack
{
    if (!mallList)
    {
        NSLog(@"getMallsWithURL: mallList is not allocated");
        return;
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                __block NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                for (int i = 0; i < [jsonArray count]; ++i)
                {
                    NSDictionary* json = jsonArray[i];
                    [mallList addObject:json];
                }
                if (blockCallBack)
                {
                    blockCallBack();
                }
            }] resume];
}

+ (void)getAdvertsWithURL:(NSString *)url selectOnly:(NSString*)mallID putTo:(NSMutableDictionary*)malls callBack:(void(^)())blockCallBack
{
    if (!malls)
    {
        NSLog(@"getAdvertsWithURL: malls is nil");
        return;
    }
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                __block NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                int tmpIndex = -1;
                for (int i = 0; i < [jsonArray count]; ++i)
                {
                    NSDictionary* json = jsonArray[i];
                    NSString *mall = [[json objectForKey:@"advMall"]objectForKey:@"mall"];
                    NSString *_id = [json objectForKey:@"_id"];
                    if ([mallID isEqual:ALLMALLS])
                    {
                        if ([malls objectForKey:mall] == nil)
                        {
                            ++tmpIndex;
                            [malls setValue:[[NSMutableArray alloc] init] forKey:mall];
                        }
                        if ([[malls objectForKey:mall] indexOfObject:json] == NSNotFound)
                        {
                            [[malls objectForKey:mall] addObject:json];
                        }
                    }
                    else if ([mall isEqualToString:mallID])
                    {
                        if ([malls objectForKey:mall] == nil)
                        {
                            [malls setValue:[[NSMutableArray alloc] init] forKey:mall];
                        }
                        if ([[malls objectForKey:mall] indexOfObject:json] == NSNotFound)
                        {
                            [[malls objectForKey:mall] addObject:json];
                        }
                    }
                }
                if (blockCallBack)
                {
                    blockCallBack();
                }
            }] resume];
}

+ (void)increaseSegueCount:(NSString *)_id
{
    NSString *url = [[NSString alloc] initWithFormat:@"%@/%@", __ADV__, _id];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:
      [NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error)
    {
        NSLog(@"%@", url);
    }] resume];
}
@end
